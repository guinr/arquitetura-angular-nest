export declare class User {
    id?: number | undefined;
    name?: string | undefined;
    email?: string | undefined;
    password?: string | undefined;
    constructor(id?: number | undefined, name?: string | undefined, email?: string | undefined, password?: string | undefined);
}
