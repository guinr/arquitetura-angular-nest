import { Injectable, NotFoundException, NotAcceptableException } from '@nestjs/common';
import { User } from '@guibrandl/angular-nest-shared';
import { v4 } from 'uuid';

@Injectable()
export class UsersService {
  private users: User[] = [];

  login(userLogin: User) {
    return this.users.findIndex(user => (user.name === userLogin.name && user.password === userLogin.password)) !== -1;
  }

  insert(user: User) {
    if (!this.isValidUser(user)) {
      throw new NotAcceptableException('Invalid fields');
    }

    user.id = v4();
    this.users.push(user);
    return user.id;
  }

  isValidUser(user: User) {
    return user.name !== undefined &&
    user.email !== undefined &&
    user.password !== undefined &&
    user.name !== '' &&
    user.email !== '' &&
    user.password !== '';
  }

  findAll(): User[] {
    return [...this.users];
  }

  findById(id: string): User {
    return {...this.getUserById(id)};
  }

  update(id: string, user: User) {
    const index = this.findUserIndex(id);
    const foundUser = this.getUserById(id);
    const updateUser = {...foundUser};
    if (user.name) { updateUser.name = user.name; }
    if (user.email) { updateUser.email = user.email; }
    if (user.password) { updateUser.password = user.password; }
    this.users[index] = updateUser;
  }

  delete(id: string) {
    const index = this.findUserIndex(id);
    this.users.splice(index, 1);
  }

  private getUserById(id: string): User {
    const index = this.findUserIndex(id);
    return this.users[index];
  }

  private findUserIndex(id: string) {
    const index = this.users.findIndex(user => user.id === id);
    if (index === -1) {
      throw new NotFoundException('User not found');
    }
    return index;
  }

}
