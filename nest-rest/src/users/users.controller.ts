import { Controller, Get, Post, Body, Param, Patch, Delete } from '@nestjs/common';
import { User } from '@guibrandl/angular-nest-shared';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

  constructor(private readonly usersService: UsersService) {}

  @Post('login')
  login(@Body() user: User) {
    return this.usersService.login(user);
  }

  @Post()
  insert(@Body() user: User) {
    return this.usersService.insert(user);
  }

  @Get()
  findAll(): User[] {
    return this.usersService.findAll();
  }

  @Get(':id')
  findUserById(@Param('id') id: string) {
    return this.usersService.findById(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() user: User,
  ) {
    this.usersService.update(id, user);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    this.usersService.delete(id);
  }

}
