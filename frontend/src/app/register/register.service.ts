import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '@guibrandl/angular-nest-shared';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  baseUrl = 'http://localhost:3000/users';

  constructor(private httpClient: HttpClient) { }

  insert(user: User) {
    return this.httpClient.post(this.baseUrl, user, { responseType: 'text' });
  }

}
