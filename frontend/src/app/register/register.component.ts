import { Component, OnInit } from '@angular/core';
import { User } from '@guibrandl/angular-nest-shared';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;
  retypedPassword: string;
  isBackToLoginVisible = false;

  constructor(private registerService: RegisterService) { }

  ngOnInit() {
    this.user = new User();
  }

  updateName(name: string) {
    this.user.name = name;
  }

  updateEmail(email: string) {
    this.user.email = email;
  }

  updatePassword(password: string) {
    this.user.password = password;
  }

  updateRetypedPassword(retypedPassword: string) {
    this.retypedPassword = retypedPassword;
  }

  isRegisterDisabled() {
    return !(this.user.name && this.user.email && this.user.password && this.user.password === this.retypedPassword);
  }

  register() {
    this.registerService.insert(this.user).subscribe(response => {
      this.isBackToLoginVisible = true;
    });
  }

}
