import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { User } from '@guibrandl/angular-nest-shared';
import { LoginService } from './login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;
  isUserOrPasswordInvalid = false;

  constructor(
    private loginService: LoginService,
    private router: Router) { }

  ngOnInit() {
    this.user = new User();
  }

  updateUsername(username: string) {
    this.user.name = username;
  }

  updatePassword(password: string) {
    this.user.password = password;
  }

  isLoginDisabled() {
    return !(this.user.name && this.user.password);
  }

  login() {
    this.loginService.login(this.user).subscribe(response => {
      if (response) {
        this.router.navigate(['/dashboard']);
      } else {
        this.isUserOrPasswordInvalid = true;
      }
    });
  }

}
